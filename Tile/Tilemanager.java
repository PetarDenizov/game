package Tile;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.imageio.ImageIO;

import Igrata.GameMapPanel;



public class Tilemanager {

	GameMapPanel gp;
	Tiles[] tile;
	public int mapTileNum[][];
	public Boolean mapTileNum1[][] = new Boolean[100][100];
	public Boolean ismovable(int x ,int y) {
		return mapTileNum1[x][y];
	}
	public void setmovable(int x ,int y,Boolean b) {
		 mapTileNum1[x][y] = b;
	}
	
	public Tilemanager(GameMapPanel gp) {
		this.gp = gp;
		
		tile = new Tiles[100];
	
		mapTileNum  = loadMap("Maps/map01.txt");
		getTileImage();
		

		
		
	}

	public void getTileImage() {
		try {
			
			tile[0] = new Tiles();
			tile[0].setCollision(false); ;
			
			tile[1] = new Tiles();
			tile[1].setCollision(true); 
			
			
			tile[10] = new Tiles();
			tile[10].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/ice.png"));
			
			tile[11] = new Tiles();
			tile[11].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterleft.png"));
			
			tile[12] = new Tiles();
			tile[12].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterleftice.png"));
					
			tile[13] = new Tiles();
			tile[13].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterright.png"));
		    
			tile[14] = new Tiles();
			tile[14].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterup.png"));
			
			tile[15] = new Tiles();
			tile[15].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterleftup.png"));
			
			tile[16] = new Tiles();
			tile[16].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterdown.png"));
			
			tile[17] = new Tiles();
			tile[17].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterdownleft.png"));
			
			tile[18] = new Tiles();
			tile[18].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/watertopright.png"));
			
			tile[19] = new Tiles();
			tile[19].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/waterdownright.png"));
			
			tile[20] = new Tiles();
			tile[20].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/water.png"));
			
			tile[21] = new Tiles();
			tile[21].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/sand.png"));
			
			tile[22] = new Tiles();
			tile[22].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/tree.png"));
			
			tile[23] = new Tiles();
			tile[23].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/grass.png"));
			tile[23].setCollision(true);
			
			tile[24] = new Tiles();
			tile[24].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/wall.png"));
		    tile[24].setCollision(true);
		    
		    tile[25] = new Tiles();
			tile[25].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stone.png"));
			
			tile[26] = new Tiles();
			tile[26].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/lava.png"));
			
			tile[27] = new Tiles();
			tile[27].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/prison1.png"));
			
			tile[28] = new Tiles();
			tile[28].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stone1.png"));
			
			tile[29] = new Tiles();
			tile[29].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stone2.png"));
			
			tile[30] = new Tiles();
			tile[30].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stoneleft.png"));
			
			tile[31] = new Tiles();
			tile[31].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/skull.png"));
			
			tile[32] = new Tiles();
			tile[32].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stoneup.png"));
			
			tile[33] = new Tiles();
			tile[33].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/stoneright.png"));
			
			tile[34] = new Tiles();
			tile[34].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/fish.png"));
			
			tile[35] = new Tiles();
			tile[35].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/forgefloor.png"));
			
            tile[37] = new Tiles();
			tile[37].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/lava1.png"));
	   
			tile[36] = new Tiles();
			tile[36].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/lavadowncenter.png"));

			
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	public int[][] loadMap(String s) {
		
	
		try {
			File f = new File(s);
		
			Scanner scan = new Scanner(f);
			
			int i = 0;
			while(scan.hasNextLine()) {
				String line = scan.nextLine();
				if(!line.equals("")) {
					i++;
				}
			}
			scan.close();
			scan = new Scanner (f);
			int[][] arr =  new int[i][];
			
			for (i=0; i<arr.length;i++) {
				String line = scan.nextLine();
				String number[] = line.split(" ");
				int[]nums = new int[number.length];
				for(int j =0;  j <number.length;j++) {
					nums[j]  = Integer.parseInt(number[j]);
					if(nums[j] == 24 || nums[j] == 22 ) {
						mapTileNum1[j][i] = false;
					}else {
						mapTileNum1[j][i] = true;
					}
				}
				
				arr[i] = nums;
				
		}
			scan.close();
			return arr;
		}catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;	
	}
	
	public void draw(Graphics g) {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int tilesizex = (int) screenSize.getWidth()/gp.karta.getMapsizex();
	int tilesizey =  (int) screenSize.getHeight()/gp.karta.getMapsizey() ;
	
	for(int i=0;i < mapTileNum.length; i++) {
		for(int j=0; j<mapTileNum[0].length; j++) {
			int x = tilesizex * j;
			int y = tilesizey * i;
			int tileNum = mapTileNum[i][j];
			g.drawImage(tile[tileNum].Image, x, y, tilesizex, tilesizey,null);
		}
	}
	
}

	
		
	
}
