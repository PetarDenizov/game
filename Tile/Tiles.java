package Tile;

import java.awt.image.BufferedImage;

public class Tiles {
	public BufferedImage Image;
	private Boolean collision = false;
	
	
	public Boolean getCollision() {
		return collision;
	}
	public void setCollision(Boolean collision) {
		this.collision = collision;
	}
}
