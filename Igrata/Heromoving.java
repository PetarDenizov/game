package Igrata;

import java.awt.event.KeyEvent;

public class Heromoving {
	GameMapPanel panel;
	private boolean upPressed, downPressed, leftPressed, rightPressed, rightAttackPressed, leftAttackPressed,
			upAttackPressed, downAttackPressed;

	public Heromoving(GameMapPanel p) {
 this.panel = p;
	}

	public void Heromovepressed(GameMapPanel panel, KeyEvent e) {
		int code = e.getKeyCode();
		panel.karta.Gameover();
		panel.karta.isVictory();
		if (panel.karta.getGameover() == false && panel.karta.isVictory() == false) {
			if (code == KeyEvent.VK_W ) {
				panel.cc.CheckCollision(panel);
				panel.npcm.bearmoving();
				if(panel.tm.ismovable(panel.karta.hero.getPosx(), panel.karta.hero.getPosy()-1)) {
					panel.hm.setUpPressed(true);
					panel.karta.hero.setPosy(panel.karta.hero.getPosy()-1);
				}   			
			}
			if (code == KeyEvent.VK_S ) {
				panel.cc.CheckCollision(panel);
				panel.npcm.bearmoving();			
				
				if(panel.tm.ismovable(panel.karta.hero.getPosx(), panel.karta.hero.getPosy()+1)) {
					panel.hm.setDownPressed(true);
					panel.karta.hero.setPosy(panel.karta.hero.getPosy() + 1);
				}
				
		}

			if (code == KeyEvent.VK_A ) {
				panel.cc.CheckCollision(panel);
				panel.npcm.bearmoving();
				
				if(panel.tm.ismovable(panel.karta.hero.getPosx()-1, panel.karta.hero.getPosy())) {
					panel.hm.setLeftPressed(true);
					panel.karta.hero.setPosx(panel.karta.hero.getPosx() - 1);
				}
			}
			if (code == KeyEvent.VK_D) {

				panel.cc.CheckCollision(panel);
				panel.npcm.bearmoving();

				if(panel.tm.ismovable(panel.karta.hero.getPosx()+1, panel.karta.hero.getPosy())) {
					panel.hm.setRightPressed(true);
					panel.karta.hero.setPosx(panel.karta.hero.getPosx() + 1);
				}
			}

			if (code == KeyEvent.VK_Q) {
				panel.hm.setRightAttackPressed(true);
				// devil health
				if (panel.karta.hero.getPosx() == panel.karta.devil.getPosx() - 1
						&& panel.karta.hero.getPosy() == panel.karta.devil.getPosy()) {
					panel.karta.devil.setHp(panel.karta.devil.getHp() - 1);
				}
				// trees health
				for (int i = 0; i < panel.karta.trees.length; i++) {

					if (panel.karta.hero.getPosx() == panel.karta.trees[i].getPosx() - 1
							&& panel.karta.hero.getPosy() == panel.karta.trees[i].getPosy()
							&& panel.karta.axe1.getAxe() == 1) {
						panel.karta.trees[i].setTreehp(panel.karta.trees[i].getTreehp() - 1);
					}

				}
				// stone health
				for (int i = 0; i < panel.karta.stone.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.stone[i].getPosx() - 1
							&& panel.karta.hero.getPosy() == panel.karta.stone[i].getPosy()
							&& panel.karta.pickaxe.getPickaxe() == 1) {
						panel.karta.stone[i].setStonehp(panel.karta.stone[i].getStonehp() - 1);
					}
				}
				// cactus health
				for (int i = 0; i < panel.karta.cactus.length / 2; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx() - 1
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy()) {
						if (panel.karta.cactus[i].getCactushp() == 1) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() - 1);
						}

					}
				}
				for (int i = 3; i < panel.karta.cactus.length; i++) {

					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx() - 1
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy()) {
						if (panel.karta.cactus[i].getCactushp() == 0) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() + 1);
						}
					}
				}

			}

			if (code == KeyEvent.VK_Q) {
				panel.hm.setLeftAttackPressed(true);
				// devil health
				if (panel.karta.hero.getPosx() == panel.karta.devil.getPosx() + 1
						&& panel.karta.hero.getPosy() == panel.karta.devil.getPosy()) {
					panel.karta.devil.setHp(panel.karta.devil.getHp() - 1);
				}
				// trees health
				for (int i = 0; i < panel.karta.trees.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.trees[i].getPosx() + 1
							&& panel.karta.hero.getPosy() == panel.karta.trees[i].getPosy()
							&& panel.karta.axe1.getAxe() == 1) {
						panel.karta.trees[i].setTreehp(panel.karta.trees[i].getTreehp() - 1);
					}
					// System.out.println(panel.karta.devil.getHp());
				}
				// stone health
				for (int i = 0; i < panel.karta.trees.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.stone[i].getPosx() + 1
							&& panel.karta.hero.getPosy() == panel.karta.stone[i].getPosy()
							&& panel.karta.pickaxe.getPickaxe() == 1) {
						panel.karta.stone[i].setStonehp(panel.karta.stone[i].getStonehp() - 1);
					}
				}
				// cactus health
				for (int i = 0; i < panel.karta.cactus.length / 2; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx() + 1
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy()) {
						if (panel.karta.cactus[i].getCactushp() == 1) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() - 1);
						}
					}
				}
				for (int i = 3; i < panel.karta.cactus.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx() + 1
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy()) {
						if (panel.karta.cactus[i].getCactushp() == 0) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() + 1);
						}
					}
				}
				// sword making
				if (panel.karta.hero.getPosx() == panel.karta.forgeitem[0].getPosx() + 1
						&& panel.karta.hero.getPosy() == panel.karta.forgeitem[0].getPosy()
						&& panel.karta.sword.getSwordmaking() == 0) {
					panel.karta.sword.setSwordmaking(1);
				}
				if (panel.karta.hero.getPosx() == panel.karta.forgeitem[1].getPosx() + 1
						&& panel.karta.hero.getPosy() == panel.karta.forgeitem[1].getPosy()
						&& panel.karta.sword.getSwordmaking() == 4) {
					panel.karta.sword.setSwordmaking(5);
				}

			}
			if (code == KeyEvent.VK_Q) {
				panel.hm.setDownAttackPressed(true);
				// devil health
				if (panel.karta.hero.getPosx() == panel.karta.devil.getPosx()
						&& panel.karta.hero.getPosy() == panel.karta.devil.getPosy() + 1) {
					panel.karta.devil.setHp(panel.karta.devil.getHp() - 1);
				}
				// trees health
				for (int i = 0; i < panel.karta.trees.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.trees[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.trees[i].getPosy() + 1
							&& panel.karta.axe1.getAxe() == 1) {
						if (panel.karta.cactus[i].getCactushp() == 1) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() - 1);
						}
					}
					// System.out.println(panel.karta.devil.getHp());
				}
				// stone health
				for (int i = 0; i < panel.karta.stone.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.stone[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.stone[i].getPosy() + 1
							&& panel.karta.pickaxe.getPickaxe() == 1) {
						panel.karta.stone[i].setStonehp(panel.karta.stone[i].getStonehp() - 1);
					}
				}
				// chest health
				if (panel.karta.hero.getPosx() == panel.karta.chest.getPosx()
						&& panel.karta.hero.getPosy() == panel.karta.chest.getPosy() + 1) {
					if (panel.karta.chest.getChestopen() < 1) {
						panel.karta.chest.setChestopen(panel.karta.chest.getChestopen() + 1);
						// System.out.println(panel.karta.chest.getChestopen());
					}
				}
				// cactus health
				for (int i = 0; i < panel.karta.cactus.length / 2; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy() + 1) {
						panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() - 1);
					}
				}
				for (int i = 3; i < panel.karta.cactus.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy() + 1) {
						if (panel.karta.cactus[i].getCactushp() == 0) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() + 1);
						}
					}
				}
				// sword making
				if (panel.karta.hero.getPosx() == panel.karta.forgeitem[2].getPosx() + 1
						&& panel.karta.forgeitem[2].getPosy() + 2 == panel.karta.hero.getPosy()
						&& panel.karta.sword.getSwordmaking() == 2
						|| panel.karta.hero.getPosx() == panel.karta.forgeitem[2].getPosx()
								&& panel.karta.forgeitem[2].getPosy() + 2 == panel.karta.hero.getPosy()
								&& panel.karta.sword.getSwordmaking() == 2) {
					panel.karta.sword.setSwordmaking(3);
				}
			}

			if (code == KeyEvent.VK_Q) {
				panel.hm.setUpAttackPressed(true);
				// devil health
				if (panel.karta.hero.getPosx() == panel.karta.devil.getPosx()
						&& panel.karta.hero.getPosy() == panel.karta.devil.getPosy() - 1) {
					panel.karta.devil.setHp(panel.karta.devil.getHp() - 1);
				}
				// trees health
				for (int i = 0; i < panel.karta.trees.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.trees[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.trees[i].getPosy() - 1
							&& panel.karta.axe1.getAxe() == 1) {
						panel.karta.trees[i].setTreehp(panel.karta.trees[i].getTreehp() - 1);
					}

				}
				// stone health
				for (int i = 0; i < panel.karta.stone.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.stone[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.stone[i].getPosy() - 1
							&& panel.karta.pickaxe.getPickaxe() == 1) {
						panel.karta.stone[i].setStonehp(panel.karta.stone[i].getStonehp() - 1);
					}
				}
				// cactus health
				for (int i = 0; i < panel.karta.cactus.length / 2; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy() - 1) {
						if (panel.karta.cactus[i].getCactushp() == 1) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() - 1);
						}
					}
				}
				for (int i = 3; i < panel.karta.cactus.length; i++) {
					if (panel.karta.hero.getPosx() == panel.karta.cactus[i].getPosx()
							&& panel.karta.hero.getPosy() == panel.karta.cactus[i].getPosy() - 1) {
						if (panel.karta.cactus[i].getCactushp() == 0) {
							panel.karta.cactus[i].setCactushp(panel.karta.cactus[i].getCactushp() + 1);
						}
					}
				}

			}
		}
		panel.repaint();
	}


	public void heromovereleased(KeyEvent e, GameMapPanel panel) {
		this.panel = panel;
		int code = e.getKeyCode();
		panel.karta.Gameover();
		panel.karta.isVictory();
		if (panel.karta.getGameover() == false && panel.karta.isVictory() == false) {
			if (code == KeyEvent.VK_W) {
				panel.hm.setDownPressed(false);
				panel.hm.setLeftPressed(false);
				panel.hm.setRightPressed(false);
			}
			if (code == KeyEvent.VK_S) {
				panel.hm.setUpPressed(false);
				panel.hm.setLeftPressed(false);
				panel.hm.setRightPressed(false);
			}
			if (code == KeyEvent.VK_A) {
				panel.hm.setUpPressed(false);
				panel.hm.setDownPressed(false);
				panel.hm.setRightPressed(false);
			}
			if (code == KeyEvent.VK_D) {
				panel.hm.setUpPressed(false);
				panel.hm.setDownPressed(false);
				panel.hm.setLeftPressed(false);

			}
			if (code == KeyEvent.VK_Q) {
				panel.hm.setRightAttackPressed(false);
				panel.hm.setLeftAttackPressed(false);
				panel.hm.setUpAttackPressed(false);
				panel.hm.setDownAttackPressed(false);
				panel.hm.setLeftAttackPressed(false);
				panel.hm.setRightAttackPressed(false);
				panel.hm.setUpAttackPressed(false);
			}

		}
	}

	public boolean isRightAttackPressed() {
		return rightAttackPressed;
	}

	public void setRightAttackPressed(boolean rightAttackPressed) {
		this.rightAttackPressed = rightAttackPressed;
	}

	public boolean isLeftAttackPressed() {
		return leftAttackPressed;
	}

	public void setLeftAttackPressed(boolean leftAttackPressed) {
		this.leftAttackPressed = leftAttackPressed;
	}

	public void setUpPressed(boolean upPressed) {
		this.upPressed = upPressed;
	}

	public boolean isDownPressed() {
		return downPressed;
	}

	public void setDownPressed(boolean downPressed) {
		this.downPressed = downPressed;
	}

	public boolean isLeftPressed() {
		return leftPressed;
	}

	public void setLeftPressed(boolean leftPressed) {
		this.leftPressed = leftPressed;
	}

	public boolean isRightPressed() {
		return rightPressed;
	}

	public void setRightPressed(boolean rightPressed) {
		this.rightPressed = rightPressed;
	}

	public boolean isUpPressed() {
		return upPressed;
	}

	public boolean isUpAttackPressed() {
		return upAttackPressed;
	}

	public void setUpAttackPressed(boolean upAttackPressed) {
		this.upAttackPressed = upAttackPressed;
	}

	public boolean isDownAttackPressed() {
		return downAttackPressed;
	}

	public void setDownAttackPressed(boolean downAttackPressed) {
		this.downAttackPressed = downAttackPressed;
	}

}
