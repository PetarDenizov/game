package Igrata;



import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import Tile.Tilemanager;
import Tile.Tiles;

import npc.npcmoving;
public class GameMapPanel extends Panel {
	 public Map karta;
	 String[] n ;
     Dialogues dg = new Dialogues(this);
     static   public Tilemanager tm ;
     GameKeyListener gkl = new GameKeyListener(this);
	 Tiles tile =  new Tiles();
	 public Checkcollision cc = new Checkcollision ();
     public npcmoving npcm = new  npcmoving (this, null);
     public Heromoving hm = new  Heromoving(this);
	 public GameMapPanel(Map k) {
		 tm = new Tilemanager(this);
		karta = k;
		addKeyListener(gkl);
		this.setBackground(Color.GREEN);
	}
		public void paint(Graphics g) {
//			 File file = new File(n[0]);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			int sizetilex = (int) screenSize.getWidth()/this.karta.getMapsizex();
			int sizetiley =  (int) screenSize.getHeight()/this.karta.getMapsizey() ;
		  karta.Gameover();
		  karta.Victory();
		  if(karta.getGameover()== false  && karta.isVictory()== false) {
			File f3 = new File("Npcimage/enemyfullhp.png");
			File f13 = new File("Npcimage/enemyhalfhp.png");
			File f14 = new File("Npcimage/enemylowhp.png");
			File f15 = new File("Npcimage/enemyzerohp.png");
			File f19 = new File("Npcimage/devildead.png");
			File f20 = new File("Npcimage/lumberjack.png");
			File f45 = new File("Npcimage/scorpion.png");
			File f46 = new File("Npcimage/blacksmith.png");
			File f42 = new File("Npcimage/gardener.png");
			File f22 = new File("Npcimage/prisonboss.png");
			File f23 = new File("Npcimage/miner.png");
			File f24 = new File("Npcimage/kid.png");
			File f26 = new File("Npcimage/cat.png");
			File f27 = new File("Npcimage/bearleft.png");
			File f28 = new File("Npcimage/bearright.png");
			File f29 = new File("Npcimage/bearup.png");
			File f30 = new File("Npcimage/beardown.png");
			File f = new File("Heroimage/heroright.png");
			File f5 = new File("Heroimage/herohpcounter.png");
			File f6 = new File("Heroimage/herohpcounter2.png");
			File f7 = new File("Heroimage/herohpcounter1.png");
			File f8 = new File("Heroimage/heroleft.png");
			File f9 = new File("Heroimage/heroup.png");
			File f10 = new File("Heroimage/herodown.png");
			File f11 = new File("Heroimage/herorightattack.png");
			File f12 = new File("Heroimage/heroleftattack.png");
			File f17 = new File("Heroimage/heroupattack.png");
			File f18 = new File("Heroimage/herodownattack.png");			
			File f32 = new File("Itemsimage/catfood.png");
			File f33 = new File("Itemsimage/treec.png");
			File f34 = new File("Itemsimage/axe.png");
			File f35 = new File("Itemsimage/treec1.png");
			File f36 = new File("Itemsimage/key.png");
			File f37 = new File("Itemsimage/chest.png");
			File f38 = new File("Itemsimage/chestopen.png");
			File f39 = new File("Itemsimage/pickaxe.png");
			File f40 = new File("Itemsimage/stone.png");
			File f41 = new File("Itemsimage/stone1.png");
			File f48 = new File("Itemsimage/forgeitem0.png");
			File f49 = new File("Itemsimage/forgeitem1.png");
			File f50 = new File("Itemsimage/forgeitem2.png");
			File f43 = new File("Itemsimage/cactus.png");
			File f44 = new File("Itemsimage/vase.png");
			File f51 = new File("Itemsimage/sword0.png");
			File f52 = new File("Itemsimage/sword1.png");
			File f53 = new File("Itemsimage/sword2.png");
			File f25 = new File("Itemsimage/apple.png");
			File f21 = new File("portal.png");
			File f47 = new File("lastportal.png");
			//draw Map tile
			tm.draw(g);
			try {
				//draw devil
				BufferedImage img3 = ImageIO.read(f3);
				BufferedImage img4 = ImageIO.read(f13);
				BufferedImage img5 = ImageIO.read(f14);
				BufferedImage img6 = ImageIO.read(f15);
				BufferedImage img7 = ImageIO.read(f19);
				BufferedImage img8 = ImageIO.read(f20);
				BufferedImage img9 = ImageIO.read(f21);
				BufferedImage img10 = ImageIO.read(f22);
				BufferedImage img11 = ImageIO.read(f23);
				BufferedImage img12 = ImageIO.read(f24);
				BufferedImage img13 = ImageIO.read(f25);
				BufferedImage img14 = ImageIO.read(f26);
				BufferedImage img15 = ImageIO.read(f27);
				BufferedImage img16 = ImageIO.read(f28);
				BufferedImage img17 = ImageIO.read(f29);
				BufferedImage img18 = ImageIO.read(f30);
				BufferedImage img20 = ImageIO.read(f32);
				BufferedImage img21 = ImageIO.read(f33);
				BufferedImage img22 = ImageIO.read(f34);
				BufferedImage img23 = ImageIO.read(f35);
				BufferedImage img24 = ImageIO.read(f36);
				BufferedImage img25 = ImageIO.read(f37);
				BufferedImage img26 = ImageIO.read(f38);
				BufferedImage img27 = ImageIO.read(f39);
				BufferedImage img28 = ImageIO.read(f40);
				BufferedImage img29 = ImageIO.read(f41);
				BufferedImage img30 = ImageIO.read(f42);
				BufferedImage img31 = ImageIO.read(f43);
				BufferedImage img32 = ImageIO.read(f44);
				BufferedImage img33 = ImageIO.read(f45);
				BufferedImage img34 = ImageIO.read(f46);
				BufferedImage img35 = ImageIO.read(f47);
				BufferedImage img36 = ImageIO.read(f48);
				BufferedImage img37 = ImageIO.read(f49);
				BufferedImage img38 = ImageIO.read(f50);
				BufferedImage img39 = ImageIO.read(f51);
				BufferedImage img40 = ImageIO.read(f52);
				BufferedImage img41 = ImageIO.read(f53);
				// devil hp
				if(this.karta.devil.getHp()==2) {
					img3 = img4;
				}else if(this.karta.devil.getHp()==1) {
					img3 = img5;
				}else if(this.karta.devil.getHp()==0) {
					img3 = img6;
				}else if(this.karta.devil.getHp()<0) {
					img3 = img7;
				}
				// draw Prison boss
				g.drawImage(img10,this.karta.prisonboss.getPosx()*sizetilex, this.karta.prisonboss.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw Devil
				g.drawImage(img3,this.karta.devil.getPosx()*sizetilex, this.karta.devil.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw lumber jack
				g.drawImage(img8,this.karta.lumberjack.getPosx()*sizetilex, this.karta.lumberjack.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw Miner
				g.drawImage(img11,this.karta.miner.getPosx()*sizetilex, this.karta.miner.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw Kid
				g.drawImage(img12,this.karta.kid.getPosx()*sizetilex, this.karta.kid.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw gardener
				g.drawImage(img30,this.karta.gardener.getPosx()*sizetilex, this.karta.gardener.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw  blacksmith
				g.drawImage(img34,this.karta.blacksmith.getPosx()*sizetilex, this.karta.blacksmith.getPosy()*sizetiley, sizetilex, sizetiley,null);
				//draw forge items
				g.drawImage(img36,this.karta.forgeitem[0].getPosx()*sizetilex, this.karta.forgeitem[0].getPosy()*sizetiley, sizetilex, sizetiley,null);
				g.drawImage(img37,this.karta.forgeitem[1].getPosx()*sizetilex, this.karta.forgeitem[1].getPosy()*sizetiley, sizetilex, sizetiley,null);
				g.drawImage(img38,this.karta.forgeitem[2].getPosx()*sizetilex, this.karta.forgeitem[2].getPosy()*sizetiley, sizetilex*2, sizetiley*2,null);
				//draw sword
				
				if(this.karta.sword.swordmaking == 1 && this.karta.trees[0].getTreecount() == 3 && this.karta.stone[0].getStonecount() == 3
						||  this.karta.sword.swordmaking == 1 && this.karta.trees[1].getTreecount() == 3 && this.karta.stone[1].getStonecount() == 3 
						||  this.karta.sword.swordmaking == 1 && this.karta.trees[2].getTreecount() == 3 && this.karta.stone[2].getStonecount() == 3) {
					g.drawImage(img39,this.karta.sword.getPosx()*sizetilex, this.karta.sword.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				 if(this.karta.sword.swordmaking == 3) {
					g.drawImage(img40,this.karta.sword.getPosx()*sizetilex, this.karta.sword.getPosy()*sizetiley, sizetilex, sizetiley,null);
				 }
		       if(this.karta.sword.swordmaking == 5) {
					g.drawImage(img41,this.karta.sword.getPosx()*sizetilex, this.karta.sword.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				
					
				//draw treec
				for(int i =0;i<this.karta.trees.length;i++) {
					if(this.karta.trees[i].getTreehp() == 2) {
						g.drawImage(img21,this.karta.trees[i].getPosx()*sizetilex, this.karta.trees[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}else if(this.karta.trees[i].getTreehp() == 1) {
						g.drawImage(img23,this.karta.trees[i].getPosx()*sizetilex, this.karta.trees[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}
				}
				//draw stone
				for(int i =0;i<this.karta.stone.length;i++) {
					if(this.karta.stone[i].getStonehp() == 2) {
						g.drawImage(img28,this.karta.stone[i].getPosx()*sizetilex, this.karta.stone[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}else if(this.karta.stone[i].getStonehp() == 1) {
						g.drawImage(img29,this.karta.stone[i].getPosx()*sizetilex, this.karta.stone[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}
				}
				//  draw cactus
				karta.cactushp();
				for(int i =0;i<this.karta.cactus.length;i++) {
					if(this.karta.cactus[i].getCactushp() == 1) {
						g.drawImage(img31,this.karta.cactus[i].getPosx()*sizetilex, this.karta.cactus[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}else if(this.karta.cactus[i].getCactushp() <= 0) {
						g.drawImage(img32,this.karta.cactus[i].getPosx()*sizetilex, this.karta.cactus[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
					}
				}
				//draw scorpion
				for(int i =0;i<this.karta.scorpion.length;i++) {
					g.drawImage(img33,this.karta.scorpion[i].getPosx()*sizetilex, this.karta.scorpion[i].getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				
				//draw axe
				if(this.karta.axe1.getAxe() !=1) {
					g.drawImage(img22,this.karta.axe1.getPosx()*sizetilex, this.karta.axe1.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				//draw Pickaxe
				if(this.karta.chest.getChestopen()==1 ) {
					g.drawImage(img27,this.karta.pickaxe.getPosx()*sizetilex, this.karta.pickaxe.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				
				
			     //draw Apple
				if(this.karta.apple.getApple1()!=1) {
					g.drawImage(img13,this.karta.apple.getPosx()*sizetilex, this.karta.apple.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				//draw cat food
				if(this.karta.catfood.getCatfood()!=1) {
					g.drawImage(img20,this.karta.catfood.getPosx()*sizetilex, this.karta.catfood.getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				//draw key 
				if(this.karta.key[0].getKeycount()!=1) {
						g.drawImage(img24,this.karta.key[0].getPosx()*sizetilex, this.karta.key[0].getPosy()*sizetiley, sizetilex, sizetiley,null);	
				}
				 if(this.karta.key[1].getKeycount()!=2){
					g.drawImage(img24,this.karta.key[1].getPosx()*sizetilex, this.karta.key[1].getPosy()*sizetiley, sizetilex, sizetiley,null);
				}
				 // draw chest 
				 if(this.karta.chest.getChestopen()==0) {
					 g.drawImage(img25,this.karta.chest.getPosx()*sizetilex, this.karta.chest.getPosy()*sizetiley, sizetilex, sizetiley,null);
						
				 }else {
					 g.drawImage(img26,this.karta.chest.getPosx()*sizetilex, this.karta.chest.getPosy()*sizetiley, sizetilex, sizetiley,null);
				 }
				 
				//draw Bear
			    if(this.karta.bear.getPosy()==17) {
			    g.drawImage(img16,this.karta.bear.getPosx()*sizetilex, this.karta.bear.getPosy()*sizetiley, sizetilex, sizetiley,null);
			    }else if(this.karta.bear.getPosx()==30) {
			    	 g.drawImage(img18, this.karta.bear.getPosx()*sizetilex, this.karta.bear.getPosy()*sizetiley, sizetilex, sizetiley,null);
			    }else if(karta.bear.getPosx()==36) {
			    	 g.drawImage(img17, this.karta.bear.getPosx()*sizetilex, this.karta.bear.getPosy()*sizetiley, sizetilex, sizetiley,null);
			    }
			    
			    
			    
			    else if(this.karta.bear.getPosy()==11 && this.npcm.isBearleft() == false){
			    	  g.drawImage(img15, this.karta.bear.getPosx()*sizetilex,this.karta.bear.getPosy()*sizetiley, sizetilex, sizetiley,null);
			    
			    }
			    		 
				//draw Cat
				g.drawImage(img14,this.karta.cat.getPosx()*sizetilex,this.karta.cat.getPosy()* sizetiley, sizetilex, sizetiley,null);
				
				//draw portal
				for (int i=0;i<this.karta.portal.length;i++) {
					g.drawImage(img9,this.karta.portal[i].getPosx()*sizetilex,this.karta.portal[i].getPosy()*sizetiley,sizetilex,sizetiley,null);
					if(i>7) {
						g.drawImage(img35,this.karta.portal[i].getPosx()*sizetilex,this.karta.portal[i].getPosy()*sizetiley,sizetilex,sizetiley,null);
					}
				}
				
				
				
			if(karta.hero.getHealth()==2) {
					f5=f6;
			}else if(karta.hero.getHealth() == 1) {
					f5=f7;
				}
				BufferedImage img2 = ImageIO.read(f5);
				g.drawImage(img2,this.karta.hero.getPosx()*sizetilex,this.karta.hero.getPosy()*sizetiley - sizetiley , sizetilex, sizetiley,null);
			
			}catch( IOException e) {
				e.printStackTrace();
			}
			//Draw Dialogue screen
			dg.Dialogue(g);
			
				
			
	try {
				
				//Hero draw
				BufferedImage img = ImageIO.read(f);
				BufferedImage img1 = ImageIO.read(f8);
				BufferedImage img2 = ImageIO.read(f9);
				BufferedImage img3 = ImageIO.read(f10);
				BufferedImage img4 = ImageIO.read(f11);
				BufferedImage img5 = ImageIO.read(f12);
				BufferedImage img6 = ImageIO.read(f17);
				BufferedImage img7 = ImageIO.read(f18);
				
				if(this.hm.isLeftPressed() == true) {
					img=img1;
				}else if(this.hm.isUpPressed()==true) {
					img = img2;
				}else if(this.hm.isDownPressed() == true) {
					img = img3;
					
				} if(this.hm.isRightAttackPressed()==true ) {
					g.drawImage(img4,karta.hero.getPosx()*sizetilex, karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);			
				} else if(this.hm.isRightAttackPressed() == false && this.karta.hero.getPosx()== this.karta.devil.getPosx()-1 && this.karta.hero.getPosy()== this.karta.devil.getPosy()){
					g.drawImage(img,karta.hero.getPosx()*sizetilex, karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);
				} else if(this.hm.isLeftAttackPressed()== true ) {
					g.drawImage(img5,karta.hero.getPosx()*sizetilex,  this.karta.hero.getPosy()*sizetiley,sizetilex,sizetiley,null);	
				}  else if(this.hm.isLeftAttackPressed() == false && this.karta.hero.getPosx()== this.karta.devil.getPosx()+1 &&this.karta.hero.getPosy()==this.karta.devil.getPosy()){
					g.drawImage(img1,karta.hero.getPosx()*sizetilex, karta.hero.getPosy()*sizetiley, sizetilex,sizetiley,null);	
					
			}else if(this.hm.isUpAttackPressed() == true) {
				g.drawImage(img6,this.karta.hero.getPosx()*sizetilex,this.karta.hero.getPosy()*sizetiley, sizetilex,sizetiley,null);
				
			}else if(this.hm.isUpAttackPressed() == false && this.karta.hero.getPosx()==this.karta.devil.getPosx() && this.karta.hero.getPosy()== this.karta.devil.getPosy()+1) {
		g.drawImage(img2,this.karta.hero.getPosx()*sizetilex, this.karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);
			}else if(this.hm.isDownAttackPressed() == true) {
				g.drawImage(img7,this.karta.hero.getPosx()*sizetilex,this.karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);
				
			}else if(this.hm.isDownAttackPressed() == false && this.karta.hero.getPosx()==karta.devil.getPosx() && this.karta.hero.getPosy()== this.karta.devil.getPosy()+1) {
		g.drawImage(img3,this.karta.hero.getPosx()*sizetilex,this.karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);
			}else {
				g.drawImage(img,this.karta.hero.getPosx()*sizetilex,this.karta.hero.getPosy()*sizetiley, sizetilex, sizetiley,null);	
			}
	}catch( IOException e) {
				e.printStackTrace();
			}
	
	
	
	
		}else if(karta.getGameover() == true) {
			File f = new File("MenuImage/Defeat.png");
			try {
				BufferedImage img = ImageIO.read(f);
				g.drawImage(img,0,0, screenSize.width ,screenSize.height-25 ,null);	
			} catch (Exception e) {
				// TODO: handle exception
			
		}
		}else if(karta.isVictory() == true) {
			File f = new File("MenuImage/Victory.png");
			try {
				BufferedImage img = ImageIO.read(f);
				g.drawImage(img,0,0, screenSize.width ,screenSize.height-25 ,null);	
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}

		}



		
}

	
	

