package Igrata;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameFrame extends Frame implements ActionListener {
	public GameMapPanel Panel;
	GameMenuPanel MenuPanel;
	Button button;
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	GameFrame(GameMapPanel p,GameMenuPanel mp) {
		Panel = p;
		MenuPanel = mp;
		// GameFrame
		this.setLayout(null);
		setTitle("Prison Escape");
		
		setSize(this.getMaximumSize());
		this.setLocation(0, 0);
		setVisible(true);
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		GameWindoWListener gwl = new GameWindoWListener();
		addWindowListener(gwl);
		// MenuPanel
		button = new Button();
		button.setBounds ((int) (0),(20),100,50);
		button.setSize(screenSize.width/10,screenSize.height/10);
		button.setVisible(true);
		button.setBackground(Color.gray);
		button.setForeground(Color.white);
		button.setLabel("Play");
		button.setFont(getFont().deriveFont(Font.CENTER_BASELINE));
		button.addActionListener(this);
		this.add(button);
		MenuPanel = new GameMenuPanel();
		MenuPanel.setBounds(0, 0, screenSize.width, screenSize.height);
		this.add(MenuPanel);
		// MapPanel
		Panel = new GameMapPanel(Panel.karta);
		Panel.setBounds(30, 30,screenSize.width-( screenSize.width/Panel.karta.getMapsizex()), screenSize.height-( screenSize.height/Panel.karta.getMapsizey()));
		this.add(Panel);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == button) {
			this.remove(MenuPanel);
			this.remove(button);
		}
	}

}
