package Igrata;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import npc.npcmoving;

public class GameKeyListener implements KeyListener {
	GameMapPanel panel;

	GameKeyListener(GameMapPanel p) {
		panel = p;

	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

		panel.hm.Heromovepressed(panel, e);

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		panel.hm.heromovereleased(e, panel);

	}
}
