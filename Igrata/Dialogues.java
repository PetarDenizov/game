package Igrata;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;

public class Dialogues {
	GameMapPanel gp;

	private String[] dialogues = new String[20];
	public String currentdialogue;

	Dialogues(GameMapPanel gp) {

		this.gp = gp;

	}

	public void setDialogue() {
		dialogues[0] = "Hello You are in my prison\nIf you want to escape\n You have to pass 8 tests\n If you fail you die";
		dialogues[1] = "Chop three trees for me";
		dialogues[2] = "Mine three stones for me";
		dialogues[3] = "Sir can you save my cat?";
		dialogues[4] = "Thank you very much for\n the help sir?";
		dialogues[5] = "Move the cactus for me please \n and be careful with the scorpions";
		dialogues[6] = "If you want to clear the next test\n you have to make your own sword \n you will need 3 wood and 3 \n iron";
		dialogues[7] = "First make your sword handle";
		dialogues[8] = "Smelt the iron for you sword";
		dialogues[9] = "Cool down your sword \n if you want to use it";
		dialogues[10] = "Good job now you can fight the boss";
	}

	public void npcspeak() {

		if (gp.karta.hero.getPosx() == gp.karta.prisonboss.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.prisonboss.getPosy()) {
			currentdialogue = dialogues[0];
		}
		if (gp.karta.hero.getPosx() == gp.karta.lumberjack.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.lumberjack.getPosy()) {
			currentdialogue = dialogues[1];
		}
		if (gp.karta.hero.getPosx() == gp.karta.miner.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.miner.getPosy()) {
			currentdialogue = dialogues[2];
		}
		if (gp.karta.hero.getPosx() == gp.karta.kid.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.kid.getPosy()) {
			currentdialogue = dialogues[3];
			if (gp.karta.cat.getPosx() == 29 && gp.karta.cat.getPosy() == 6) {
				currentdialogue = dialogues[4];
			}
		}

		if (gp.karta.hero.getPosx() == gp.karta.gardener.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.gardener.getPosy()) {
			currentdialogue = dialogues[5];
		}
		if (gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx()
				&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy() + 1) {
			currentdialogue = dialogues[6];
		}
	}

	public void drawDialoguescreen(Graphics g) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int sizetilex = (int) screenSize.getWidth() / gp.karta.getMapsizex();
		int sizetiley = (int) screenSize.getHeight() / gp.karta.getMapsizey();
		if (gp.karta.hero.getPosx() == gp.karta.prisonboss.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.prisonboss.getPosy()) {

			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * gp.karta.prisonboss.getPosx(), sizetiley * (gp.karta.prisonboss.getPosy() - 2),
					300, 100, 35, 35);
			c = new Color(255, 255, 255, 255);
		} else if (gp.karta.hero.getPosx() == gp.karta.lumberjack.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.lumberjack.getPosy()) {
			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * gp.karta.lumberjack.getPosx(), sizetiley * (gp.karta.lumberjack.getPosy() - 2),
					300, 100, 35, 35);
			c = new Color(255, 255, 255, 255);
		} else if (gp.karta.hero.getPosx() == gp.karta.miner.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.miner.getPosy()) {
			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * gp.karta.miner.getPosx(), sizetiley * (gp.karta.miner.getPosy() - 2), 300, 100,
					35, 35);
			c = new Color(255, 255, 255, 255);
		} else if (gp.karta.hero.getPosx() == gp.karta.kid.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.kid.getPosy()) {
			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * (gp.karta.kid.getPosx()+2), sizetiley * (gp.karta.kid.getPosy() - 2), 300, 100, 35,
					35);
			c = new Color(255, 255, 255, 255);
		} else if (gp.karta.hero.getPosx() == gp.karta.gardener.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.gardener.getPosy()) {
			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * gp.karta.gardener.getPosx(), sizetiley * (gp.karta.gardener.getPosy() - 2), 320,
					100, 35, 35);
			c = new Color(255, 255, 255, 255);
		} else if (gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx()
				&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy() + 1
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[0].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[0].getPosy()
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
						&& gp.karta.sword.getSwordmaking() == 2
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
						&& gp.karta.sword.getSwordmaking() == 2
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[1].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[1].getPosy()
						&& gp.karta.sword.getSwordmaking() == 4
				|| gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx() - 1
						&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy()
						&& gp.karta.sword.getSwordmaking() == 6) {
			Color c = new Color(0, 0, 0, 200);
			g.setColor(c);
			g.fillRoundRect(sizetilex * gp.karta.blacksmith.getPosx(), sizetiley * (gp.karta.blacksmith.getPosy() - 2),
					300, 100, 35, 35);
			c = new Color(255, 255, 255, 255);
		}
	}

	public void Dialogue(Graphics g) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int sizetilex = (int) screenSize.getWidth() / gp.karta.getMapsizex();
		int sizetiley = (int) screenSize.getHeight() / gp.karta.getMapsizey();

		setDialogue();
		npcspeak();
		;
		if (gp.karta.hero.getPosx() == gp.karta.prisonboss.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.prisonboss.getPosy()) {
			drawDialoguescreen(g);
			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				g.drawString(line, sizetilex * gp.karta.prisonboss.getPosx(),
						(int) (sizetiley * (gp.karta.prisonboss.getPosy() - 1.5)));
				sizetiley += (sizetiley / 2 + 10 ) ;
			}
		} else if (this.gp.karta.hero.getPosx() == this.gp.karta.lumberjack.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.lumberjack.getPosy()) {
			drawDialoguescreen(g);

			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				g.drawString(line, sizetilex * gp.karta.lumberjack.getPosx(),
						sizetiley * (gp.karta.lumberjack.getPosy() - 1));
				sizetiley += (sizetiley / 2) - 10;
			}
		} else if (gp.karta.hero.getPosx() == gp.karta.miner.getPosx() - 1
				&& gp.karta.hero.getPosy() == gp.karta.miner.getPosy()) {
			drawDialoguescreen(g);

			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				g.drawString(line, sizetilex * gp.karta.miner.getPosx(), sizetiley * (gp.karta.miner.getPosy() - 1));
				sizetiley += (sizetiley / 2) - 10;
			}
		} else if (gp.karta.hero.getPosx() == gp.karta.kid.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.kid.getPosy()) {
			drawDialoguescreen(g);
			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				g.drawString(line, sizetilex * (gp.karta.kid.getPosx() + 2), sizetiley * (gp.karta.kid.getPosy() - 1));
				sizetiley += 5;
			}

		} else if (gp.karta.hero.getPosx() == gp.karta.gardener.getPosx() + 1
				&& gp.karta.hero.getPosy() == gp.karta.gardener.getPosy()) {
			drawDialoguescreen(g);
			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				g.drawString(line, sizetilex * (gp.karta.gardener.getPosx()),
						(int) (sizetiley * (gp.karta.gardener.getPosy() - 1.5)));
				sizetiley += 5;
			}
		} else if (gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx()
				&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy() + 1
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[0].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[0].getPosy()
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
						&& gp.karta.sword.getSwordmaking() == 2
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
						&& gp.karta.sword.getSwordmaking() == 2
				|| gp.karta.hero.getPosx() == gp.karta.forgeitem[1].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[1].getPosy()
						&& gp.karta.sword.getSwordmaking() == 4
				|| gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx() - 1
						&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy()
						&& gp.karta.sword.getSwordmaking() == 6) {
			drawDialoguescreen(g);
			g.setFont(g.getFont().deriveFont(Font.CENTER_BASELINE, 20));
			g.setColor(new Color(255, 0, 0));
			for (String line : currentdialogue.split("\n")) {
				if (gp.karta.hero.getPosx() == gp.karta.forgeitem[0].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[0].getPosy()
						&& gp.karta.sword.getSwordmaking() == 0) {
					currentdialogue = dialogues[7];
				}
				if (gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
						&& gp.karta.sword.getSwordmaking() == 2
						|| gp.karta.hero.getPosx() == gp.karta.forgeitem[2].getPosx()
								&& gp.karta.hero.getPosy() == gp.karta.forgeitem[2].getPosy() + 2
								&& gp.karta.sword.getSwordmaking() == 2) {
					currentdialogue = dialogues[8];
				}
				if (gp.karta.hero.getPosx() == gp.karta.forgeitem[1].getPosx() + 1
						&& gp.karta.hero.getPosy() == gp.karta.forgeitem[1].getPosy()
						&& gp.karta.sword.getSwordmaking() == 4) {
					currentdialogue = dialogues[9];
				}
				if (gp.karta.hero.getPosx() == gp.karta.blacksmith.getPosx() - 1
						&& gp.karta.hero.getPosy() == gp.karta.blacksmith.getPosy()
						&& gp.karta.sword.getSwordmaking() == 6) {
					currentdialogue = dialogues[10];
				}
				g.drawString(line, sizetilex * (gp.karta.blacksmith.getPosx()),
						(int) (sizetiley * (gp.karta.blacksmith.getPosy() - 1.5)));
				sizetiley += 2;
			}
		}
	}
}
