package Igrata;
import java.io.IOException;

import Items.Axe;
import Items.Cactus;
import Items.Catfood;
import Items.Chest;
import Items.Forgeitems;
import Items.Heroitems;
import Items.Pickaxe;
import Items.Stone;
import Items.Sword;
import Items.Apple;
import Items.Trees;
import npc.Bear;
import npc.Blacksmith;
import npc.Cat;
import npc.Devil;
import npc.Gardener;
import npc.Lumberjack;
import npc.Miner;
import npc.Portal;
import npc.Prisonboss;
import npc.Scorpion;
import npc.kid;
import npc.npcmoving;

public class Map {
	GameMapPanel p = new GameMapPanel(this);
	private int mapsizex;
	private int mapsizey;

	public Hero hero;
	public Devil devil;
	public Lumberjack lumberjack;
	public Prisonboss prisonboss;
	public Miner miner;
	public kid kid;
	public Apple apple;
	public Cat cat;
	public Bear bear;
	public Trees[] trees;
	public Items.Key[] key;
	public Stone[] stone;
	public Catfood catfood;
	public Axe axe1;
	public Chest chest;
	public Pickaxe pickaxe;
	public Gardener gardener;
	public Blacksmith blacksmith;
	public Cactus[] cactus;
	public Scorpion[] scorpion;
	public Portal[] portal;
	public Forgeitems[] forgeitem;
	public Sword sword;
	public Heroitems hitems = new Heroitems(p);
	public npcmoving npcm = new  npcmoving (p, hitems);

	private boolean Gameover;
	private boolean Victory;

	public Map(int x, int y) {
		mapsizex = x;
		mapsizey = y;

	}

	public void startgame(GameMapPanel p) throws InterruptedException, IOException {

		this.Gameover();
		this.Victory();
		if ( this.isVictory() == false && this.getGameover() == false) {

			while (true) {
				this.start();
				
			}

		}
	}

	public void start() {
		portals();
		npcm.movingcat();
		hitems.herosea();
		npcm.bearhitting();
		hitems.axe();
		hitems.Pickaxe();
		npcm.movingscorpions();
		hitems.treecut();
		hitems.stonecut();
		hitems.sword();
		herolava();
		hitems.apple();
		hitems.key();
	
		for(int i=0;i<trees.length;i++) {
			if(trees[i].getTreehp()<=0 ) {
				
			p.hm.panel.tm.setmovable(trees[i].getPosx(),trees[i].getPosy(),true);
			}else {
				p.hm.panel.tm.setmovable(trees[i].getPosx(),trees[i].getPosy(),false);
			}
		}
			for(int i = 0;i<stone.length;i++) {
				if(stone[i].getStonehp()<=0 ) {
				p.hm.panel.tm.setmovable(stone[i].getPosx(),stone[i].getPosy(),true);
				}else {
					p.hm.panel.tm.setmovable(stone[i].getPosx(),stone[i].getPosy(),false);
				}
		}
		
	}
	

	public void portals() {
		
		hitems.treecut();
hitems.stonecut();

		if (hero.getPosx() == portal[0].getPosx() && hero.getPosy() == portal[0].getPosy()) {
			hero.setPosx(portal[1].getPosx() + 1);
			hero.setPosy(portal[1].getPosy());
		}
		if (hero.getPosx() == portal[1].getPosx() && hero.getPosy() == portal[1].getPosy()
				&& key[0].getKeycount() == 1) {
			hero.setPosx(portal[2].getPosx() + 1);
			hero.setPosy(portal[2].getPosy());
		}
		if (hero.getPosx() == portal[2].getPosx() && hero.getPosy() == portal[2].getPosy()
				&& key[1].getKeycount() == 2) {
			hero.setPosx(portal[3].getPosx() + 1);
			hero.setPosy(portal[3].getPosy());
		}
		if (hero.getPosx() == portal[3].getPosx() && hero.getPosy() == portal[3].getPosy() && cat.getPosx() == 29
				&& cat.getPosy() == 6 && catfood.getCatfood() == 1) {
			hero.setPosx(portal[4].getPosx());
			hero.setPosy(portal[4].getPosy() + 1);
		}
		if (hero.getPosx() == portal[4].getPosx() && hero.getPosy() == portal[4].getPosy() && apple.getApple1() == 1) {
			hero.setPosx(portal[5].getPosx() + 1);
			hero.setPosy(portal[5].getPosy());
		}
		if (hero.getPosx() == portal[5].getPosx() && hero.getPosy() == portal[5].getPosy()
				&& cactus[3].getCactushp() == 1 && cactus[4].getCactushp() == 1 && cactus[5].getCactushp() == 1) {
			hero.setPosx(portal[6].getPosx() + 1);
			hero.setPosy(portal[6].getPosy());
		}
		for (int i = 0; i < stone.length; i++) {
			if (hero.getPosx() == portal[6].getPosx() && hero.getPosy() == portal[6].getPosy()
					&& stone[i].getStonecount() == 3 && trees[i].getTreecount() == 3 && sword.getSwordmaking() == 6) {
				hero.setPosx(portal[7].getPosx() + 1);
				hero.setPosy(portal[7].getPosy());
			}
		}

		if (hero.getPosx() == portal[8].getPosx() && hero.getPosy() == portal[8].getPosy()) {
			hero.setPosx(portal[9].getPosx() + 1);
			hero.setPosy(portal[9].getPosy());
		}

	}

	

	public void cactushp() {
		for (int j = 0; j < cactus.length / 2; j++) {
			for (int i = 3; i < cactus.length; i++) {
				if (cactus[j].getCactushp() == 1) {
					cactus[i].setCactushp(0);
				}

			}
		}

	}

	

	public void herolava() {
		if (hero.getPosx() < 10 && hero.getPosy() > 9 && hero.getPosy() != 10 && hero.getPosy() != 18
				&& hero.getPosy() != 14 && hero.getPosx() != 1 && hero.getPosx() != 5 && hero.getPosx() != 9) {
			hero.setPosx(5);
			hero.setPosy(15);
			hero.setHealth(hero.getHealth() - 1);
		}

	}
	public void Gameover() {
		hitems.herosea();
		if (hero.getHealth() <= 0) {
			this.setGameover(true);
		} else {
			this.setGameover(false);
		}
	}

	public void Victory() {

		if (devil.getHp() <= 0) {
			this.setVictory(true);
		} else {
			this.setVictory(false);
		}
	}
	
	

	

	public int getMapsizex() {
		return mapsizex;
	}

	public void setMapsizex(int mapsizex) {
		this.mapsizex = mapsizex;
	}

	public int getMapsizey() {
		return mapsizey;
	}

	public void setMapsizey(int mapsizey) {
		this.mapsizey = mapsizey;
	}

	public boolean getGameover() {
		return Gameover;
	}

	public void setGameover(boolean b) {
		Gameover = b;
	}

	public boolean isVictory() {
		return Victory;
	}

	public void setVictory(boolean victory) {
		Victory = victory;
	}

}
