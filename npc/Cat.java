package npc;

public class Cat extends NPC {
	
	private boolean movingcat;
	
	public Cat(int x, int y ) {
		setPosx(x);
		setPosy(y);
		
	}
	
	
	public boolean getMovingcat() {
		return movingcat;
	}
	public void setMovingcat(boolean movingcat) {
		this.movingcat = movingcat;
	}
}
