package Items;

public class Sword extends Items {
	public int swordmaking = 0;

	public int getSwordmaking() {
		return swordmaking;
	}

	public void setSwordmaking(int swordmaking) {
		this.swordmaking = swordmaking;
	}

	public Sword(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

}
