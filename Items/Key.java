package Items;

import npc.NPC;

public class Key extends Items {
	private int keycount = 0;

	public Key(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getKeycount() {
		return keycount;
	}

	public void setKeycount(int keycount) {
		this.keycount = keycount;
	}

}
