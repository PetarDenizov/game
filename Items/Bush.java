package Items;


public class Bush  extends Items {
	private int posx;
	private int posy;
	
	public Bush(int x, int y) {
		posx = x;
		posy = y;
	}
	
	
	

public int getPosx() {
		return posx;
	}
	public void setPosx(int posx) {
		this.posx = posx;
	}
	public int getPosy() {
		return posy;
	}
	public void setPosy(int posy) {
		this.posy = posy;
	}

}
