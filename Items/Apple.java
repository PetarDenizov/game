package Items;

public class Apple extends Items {

	private int apple1 = 0;

	public Apple(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getApple1() {
		return apple1;
	}

	public void setApple1(int apple1) {
		this.apple1 = apple1;
	}
}
