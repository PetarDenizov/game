package Items;

import npc.NPC;

public class Catfood extends NPC {
	private int catfood=0;
	public Catfood(int x, int y) {
		setPosx(x);
		setPosy(y);
	}
	public int getCatfood() {
		return catfood;
	}
	public void setCatfood(int catfood) {
		this.catfood = catfood;
	}
}
