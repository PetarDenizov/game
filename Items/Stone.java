package Items;

public class Stone extends Items {
	private int stonehp = 2;
	private int stonecount = 0;

	public Stone(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getStonehp() {
		return stonehp;
	}

	public void setStonehp(int stonehp) {
		this.stonehp = stonehp;
	}

	public int getStonecount() {
		return stonecount;
	}

	public void setStonecount(int stonecount) {
		this.stonecount = stonecount;
	}
}
