package Items;

public class Axe extends Items {
	private int axe = 0;

	public Axe(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getAxe() {
		return axe;
	}

	public void setAxe(int axe) {
		this.axe = axe;
	}
}
