package Items;

public class Trees extends Items {
	private int treehp = 2;
	private int treecount = 0;

	public Trees(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getTreehp() {
		return treehp;
	}

	public void setTreehp(int treehp) {
		this.treehp = treehp;
	}

	public int getTreecount() {
		return treecount;
	}

	public void setTreecount(int treecount) {
		this.treecount = treecount;
	}
}
