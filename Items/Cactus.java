package Items;

public class Cactus extends Items {
	private int cactushp = 1;

	public Cactus(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getCactushp() {
		return cactushp;
	}

	public void setCactushp(int cactushp) {
		this.cactushp = cactushp;
	}
}
