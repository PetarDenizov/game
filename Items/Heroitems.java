package Items;

import Igrata.GameMapPanel;

public class Heroitems {
	GameMapPanel panel;
	public Heroitems(GameMapPanel panel) {
		this.panel = panel;
	}
	public void apple() {

		if (panel.karta.hero.getPosx() == panel.karta.apple.getPosx() && panel.karta.hero.getPosy() == panel.karta.apple.getPosy()) {
			if (panel.karta.apple.getApple1() < 1) {
				panel.karta.apple.setApple1(+1);
			}
		}
	}

	public void catfood() {
		if (panel.karta.hero.getPosx() == panel.karta.catfood.getPosx() && panel.karta.hero.getPosy() == panel.karta.catfood.getPosy()) {
			if (panel.karta.catfood.getCatfood() < 1) {
				panel.karta.catfood.setCatfood(+1);
			}
		}

	}

	public void sword() {
		if (panel.karta.hero.getPosx() == panel.karta.sword.getPosx() && panel.karta.hero.getPosy() == panel.karta.sword.getPosy() && panel.karta.sword.getSwordmaking() == 1) {
			panel.karta.sword.setSwordmaking(2);

		}
		if (panel.karta.sword.getSwordmaking() == 3) {
			panel.karta.sword.setPosx(17);
			panel.karta.sword.setPosy(11);
		}
		if (panel.karta.hero.getPosx() == panel.karta.sword.getPosx() && panel.karta.hero.getPosy() == panel.karta.sword.getPosy() && panel.karta.sword.getPosx() == 17
				&& panel.karta.sword.getPosy() == 11) {
			panel.karta.sword.setSwordmaking(4);
		}

		if (panel.karta.sword.getSwordmaking() == 5) {
			panel.karta.sword.setPosx(11);
			panel.karta.sword.setPosy(15);
		}

		if (panel.karta.hero.getPosx() == panel.karta.sword.getPosx() && panel.karta.hero.getPosy() == panel.karta.sword.getPosy() && panel.karta.sword.getPosx() == 11
				&& panel.karta.sword.getPosy() == 15 && panel.karta.sword.getSwordmaking() == 5) {
			panel.karta.sword.setSwordmaking(6);
		}
		
	}

	public void key() {

		if (panel.karta.hero.getPosx() == panel.karta.key[0].getPosx() && panel.karta.hero.getPosy() == panel.karta.key[0].getPosy()) {
			if (panel.karta.key[0].getKeycount() < 1) {
				panel.karta.key[0].setKeycount(1);
			}
		}
		if (panel.karta.hero.getPosx() == panel.karta.key[1].getPosx() && panel.karta.hero.getPosy() == panel.karta.key[1].getPosy()) {
			if (panel.karta.key[1].getKeycount() < 2) {
				panel.karta.key[1].setKeycount(2);
			}

		}

	}

	public void axe() {
		if (panel.karta.hero.getPosx() == panel.karta.axe1.getPosx() && panel.karta.hero.getPosy() == panel.karta.axe1.getPosy()) {
			if (panel.karta.axe1.getAxe() < 1) {
				panel.karta.axe1.setAxe(+1);
			}
		}

	}

	public void Pickaxe() {

		if (panel.karta.hero.getPosx() == panel.karta.pickaxe.getPosx() && panel.karta.hero.getPosy() == panel.karta.pickaxe.getPosy() && panel.karta.chest.getChestopen() == 1) {
			if (panel.karta.pickaxe.getPickaxe() < 1) {
				panel.karta.pickaxe.setPickaxe(panel.karta.pickaxe.getPickaxe() + 1);
				panel.karta.chest.setChestopen(panel.karta.chest.getChestopen() + 1);
			}
		}

	}
	public void treecut() {

		if (panel.karta.trees[0].getTreehp() <= 0 && panel.karta.trees[1].getTreehp() <= 0 && panel.karta.trees[2].getTreehp() <= 0) {
			panel.karta.trees[0].setTreecount(3);
			panel.karta.trees[1].setTreecount(3);
			panel.karta.trees[2].setTreecount(3);
			// System.out.println(trees[i].getTreecount());
		}

	}

	public void stonecut() {

		if (panel.karta.stone[0].getStonehp() <= 0 && panel.karta.stone[1].getStonehp() <= 0 && panel.karta.stone[2].getStonehp() <= 0) {
			panel.karta.stone[0].setStonecount(3);
			panel.karta.stone[1].setStonecount(3);
			panel.karta.stone[2].setStonecount(3);
		}

	}


	public void herosea() {
		if (panel.karta.hero.getPosx() > 29 && panel.karta.hero.getPosy() < 9 && panel.karta.hero.getPosx() != 30 && panel.karta.hero.getPosy() != 3
				&& panel.karta.hero.getPosx() < 37) {
			panel.karta.hero.setPosx(29);
			panel.karta.hero.setPosy(3);
			panel.karta.hero.setHealth(panel.karta.hero.getHealth() - 1);

		}
	}

}
